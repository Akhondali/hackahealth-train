package org.diostudio.hackahealth.Fast;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Pair;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Fast extends Application {

	private static final String TAG = "Fast Class";
	public final Context con;
    private SQLiteDatabase mDb;
    private DataBaseHelper mDbHelper;
	public static String baseURL = "http://192.168.19.107/wp/public/";
	public Cursor FC;


	//Animations

	private static final float ROTATE_FROM = 0.0f;
	private static final float ROTATE_TO = -10.0f * 360.0f;// 3.141592654f * 32.0f;
	public RotateAnimation anime_rotate = new RotateAnimation(ROTATE_FROM, ROTATE_TO, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 0.5f);

	public Animation anime_fadein;
	public Animation anime_fadeOut;



	public String[] weekday ={
			"شنبه",
			"یک‌شنبه",
			"دوشنبه",
			"سه‌شنبه",
			"چهار‌شنبه",
			"پنج‌شنبه",
			"جمعه"
	};

	public Fast(Context mContext){
		this.con = mContext;
		mDbHelper = new DataBaseHelper(mContext);
	}

	/*Animate - IDK
	*
	* Animation anime_fadein = new AlphaAnimation(0, 1);
		anime_fadein.setInterpolator(new DecelerateInterpolator()); //add this
		anime_fadein.setDuration(1000);

		Animation anime_fadeOut = new AlphaAnimation(1, 0);
		anime_fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
		anime_fadeOut.setStartOffset(1000);
		anime_fadeOut.setDuration(1000);


	* */

	
	public void toast(String msg){
		Toast.makeText(con, msg, Toast.LENGTH_SHORT).show();
	}
	public void toast(String msg,boolean longTime){
		Toast.makeText(con, msg, longTime ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
	}
	
	public int s2i(String str){
		return Integer.parseInt(str);
	}

	public float s2f(String str){
		return Float.parseFloat(str);
	}




	public void intent(Context from ,Class<?> gotoclass){
		//intent(gotoclass,null);
		try {
			intent((new Intent(con, gotoclass.getClass() )), new String[]{"alaki?"});
			//new Intent(this.con,gotoclass);
		}catch (Exception e){
			Log.d("Intent error ",e.getMessage());
		}
	}
	
	public void intent(Intent in,String... data){
		//Intent in = new Intent(con, gotoclass);
		Intent myin = in;
		for(String d:data)
			in.setData(Uri.parse(d));
		startActivity(myin);
	}

	private void intent(String action,String... data){
		intent(new Intent(action), data);
	}

	public void dial(String phone){
		//intent(Intent.ACTION_DIAL, new String[]{"tel:" + phone});
		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:"+phone));
		startActivity(intent);
	}


	
	public boolean checkDataBase(){
		return mDbHelper.checkDataBase();
	}
	
	
	public void createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();

        } 
        catch (IOException mIOException) 
        {
            Log.e("SQL: ", mIOException.toString() + " UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
    }
	
	

	public void open() throws SQLException 
    {
        try 
        {
            try {
				mDbHelper.openDataBase();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException) 
        {
            Log.e("SQL db: ", "open >>"+ mSQLException.toString());
            throw mSQLException;
        }

    }

    public void close(){
        mDbHelper.close();
    }


     public Cursor getTestData() {
         try
         {
             String sql = "SELECT * FROM doctors";

             Cursor mCur = mDb.rawQuery(sql, null);
             if (mCur!=null)
             {
                mCur.moveToNext();
             }
             return mCur;
         }
         catch (SQLException mSQLException) 
         {
             Log.e("SQL db: ", "getTestData >>"+ mSQLException.toString());
             throw mSQLException;
         }
     }
	
	
	//-------------
	
	
	
	public boolean SQL_setup(String src){
		return false;
	}
	
	public boolean query(String query){
		try{
			//Execute it, and assign value to this.cursor 
			FC = mDb.rawQuery(query, null);	
			return !FC.isAfterLast();
		}catch(Exception e){
			return false;
		}
	}
	
	
	public int get_rows_count(){
		return FC.getCount();
	}


	/*
	This function will accept column names and values in pair
	Then create a search query and execute it using current database
	Returns false on failure
	 */
	public boolean search(int limit,Pair<String,String>... pairs){
		try{
			//Create search Query
			String query="SELECT * FROM doctors WHERE ";
			
			for(Pair<String,String> p : pairs )
				query+=p.first + " LIKE '%" + p.second+"%'    AND ";
			
			query = query.substring(0, query.length() -5) + ((limit != 0) ? (" LIMIT " + limit) : "") ;
			
			//Execute it, and set Fast class cursor to it 
			FC = mDb.rawQuery(query, null);
			
			//if(FC!=null)
				//FC.moveToNext();
			
			//return FC.isAfterLast() ? false : true;
			return !FC.isAfterLast();
		}catch(Exception e){
			//return e.getMessage();
			return false;
		}
		
		
	}	
	
	
	public boolean readrow(){
		//if(FC.isLast())
			//return false;
		return FC.moveToNext();
		//return true;
	}

	public boolean hasNext(){
		//if(FC.isLast())
		//return false;
		return !FC.isLast();
		//return true;
	}
	
	public String get(String column_name){
		return FC.getString(FC.getColumnIndex(column_name));
	}

	public String get(int column_id){
		return FC.getString(column_id);
	}
	
	
	
	public Pair<List<String>,List<String>> get_provinces(){
		
		List<String> id = new ArrayList<String>();
		List<String> provinces = new ArrayList<String>();
		
		id.add("-1");
		provinces.add("تمامی استان ها");
		
		if(query("SELECT id,name FROM province"))
			while(readrow()){
				id.add(get("id"));
				provinces.add(get("name"));
			}
		
		return new Pair<List<String>,List<String>>(id,provinces);
	}
	
	
	public Pair<List<String>,List<String>> get_cities(String province_id){
		List<String> id = new ArrayList<String>();
		List<String> city = new ArrayList<String>();
		
		id.add("-1");
		city.add("تمامی شهر ها");
		
		if(query("SELECT id,name FROM city WHERE city.province_id ="+province_id))
			while(readrow()){
				id.add(get("id"));
				city.add(get("name"));
			}
		return new Pair<List<String>,List<String>>(id,city) ;
	}
	
	
	public Pair<List<String>,List<String>> get_reshte(){

		List<String> id = new ArrayList<String>();
		List<String> reshte = new ArrayList<String>();
		
		id.add("-1");
		reshte.add("تمامی رشته ها");
		
		if(query("SELECT id,name FROM reshte"))
			while(readrow()){
				id.add(get("id"));
				reshte.add(get("name"));
			}
		
		return new Pair<List<String>,List<String>>(id,reshte) ;
	}
	
	
	
	public Pair<List<String>,List<String>> get_profession(String reshte_id){
		List<String> prof = new ArrayList<String>();
		List<String> profession = new ArrayList<String>();
		
		prof.add("-1");
		profession.add("تمامی تخصص ها");
		
		if(query("SELECT name FROM profession WHERE reshte_id = " + reshte_id))
			while(readrow()){
				prof.add(get("name"));
				profession.add(get("name"));
			}
		return new Pair<List<String>,List<String>>(prof,profession) ;
	}
	
	
	
	public Pair<List<String>,List<String>> get_areas(String city_id){
		List<String> id = new ArrayList<>();
		List<String> area = new ArrayList<>();

		id.add("-1");
		area.add("تمامی مناطق");
		
		
		if(query("SELECT id,name FROM area WHERE city_id = " + city_id))
			while(readrow()){
				id.add(get("id"));
				area.add(get("name"));
			}
		return new Pair<List<String>,List<String>>(id,area) ;
	}
	
	
	
	public void setList(Spinner spn,List<String> list){
        spn.setAdapter(new ArrayAdapter<String>(con, R.layout.spinner_custom,list));
	}
	
	
	
	public String zero_pad(String in){
		String zero="";
		for(int i=6-in.length();i>0;i--)
			zero+="0";

		return zero + in;
	}


	public static boolean isNumeric(String str)
	{
		try{
			double d = Double.parseDouble(str);
		}
		catch(NumberFormatException nfe){
			return false;
		}
		return true;
	}



	public boolean isInternetConnected(Context ctx) {
/*
		ConnectivityManager cm =
				(ConnectivityManager) getSystemService(con.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
		*/
		/*
		try {
			InetAddress ipAddr = InetAddress.getByName("www.google.com"); //You can replace it with your name

			if (ipAddr.equals("")) {
				return false;
			} else {
				return true;
			}

		} catch (Exception e) {
			return false;
		}*/

		/*
		boolean status=false;
		try{
			ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getNetworkInfo(0);
			if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
				status= true;
			}else {
				netInfo = cm.getNetworkInfo(1);
				if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
					status= true;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return status;
*/
/*
		Runtime runtime = Runtime.getRuntime();
		try {

			Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
			int     exitValue = ipProcess.waitFor();
			return (exitValue == 0);

		} catch (IOException e)          { e.printStackTrace(); }
		catch (InterruptedException e) { e.printStackTrace(); }

		return false;
*/
			NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx
					.getSystemService(Context.CONNECTIVITY_SERVICE))
					.getActiveNetworkInfo();
			if (info == null || !info.isConnected()) {
				return false;
			}
			return true;



	}

	public String checkNetworkStatus(Context context) {

		String networkStatus = "";

		// Get connect mangaer
		final ConnectivityManager connMgr = (ConnectivityManager)
				context.getSystemService(Context.CONNECTIVITY_SERVICE);

		// check for wifi
		final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		// check for mobile data
		final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		if( wifi.isAvailable() ) {
			networkStatus = "wifi";
		} else if( mobile.isAvailable() ) {
			networkStatus = "mobileData";
		} else {
			networkStatus = "!";
		}

		return networkStatus;

	}


	public void sync() throws Exception{

		String stt = checkNetworkStatus(con);
		if (!stt.equals("noNetwork")) {
			boolean weCanUpdate = false;

			open();
			query("SELECT value FROM setting WHERE name ='update_on_wifi'");
			if (readrow() && get("value").equals("1"))
				weCanUpdate = true;
			query("SELECT value FROM setting WHERE name='update_on_data'");
			if (readrow() && get("value").equals("1"))
				weCanUpdate = true;
			if(weCanUpdate){
				query("SELECT value FROM setting WHERE name='last_id'");
				readrow();
				String last_id = get("value");
				Ion.with(con)
						.load("http://192.168.0.103/selinux/sync.php?id="+last_id)
						.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result){
						if(result != null && e == null) {
							Log.d(TAG, "onCompleted " + "entered download" + (e != null ? ", error:"+e.getMessage() : ""));
							Sync s = new Gson().fromJson(result, Sync.class);
							int size = s.query.size();
							query("BEGIN TRANSACTION;");
							for (int i = 0; i < size; i++) {
								query(s.query.get(i).q);
								//Log.d(TAG, "Query: " + s.query.get(i).q);
							}
							query("COMMIT;");
						}
					}

					class Sync  {
						public List<query> query;
						class query {
							public String q;
						}
					}
				});

			}

		}
	}



	public Drawable get_drawable(int id) {
		final int version = Build.VERSION.SDK_INT;
		if (version >= 21) {
			return ContextCompat.getDrawable(con, id);
		} else {
			return con.getResources().getDrawable(id);
		}
	}


	public void intenet_package(String package_name,String uri){
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(uri));
		intent.setPackage(package_name);
	}








}
